# Instructions #

Welcome to the programming challenge from Maypole.

You will find the questions in the **sub-directories**. Please commit your answers (in **new files or folders**) the way **you normally do in the production environment**.

After you finish, run the following command and send the submission file to jobs@maypole.tv with your contact info.

```bash
git bundle create ../submission.bundle --all
```

Please take your time to demonstrate your competence. Thank you! Let's get started!