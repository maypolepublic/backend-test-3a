Given a height map ( height is an array ) of a land ( m x n ), please calculate how much water it can hold after a heavy rain.

![Illustration](https://storage.googleapis.com/public-assets-maypole/interview-assets/image%200.png)

```
int water_volume(const int height*, int m, int n){
    // please fill in this part
}

// For example:

int *height_example = {5, 5, 5, 
        5, 1, 5, 
        5, 5, 5};

assert (water_volume(height_example, 3, 3) == 4 ); 
```