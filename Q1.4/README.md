Please plan the product listing & product editing pages for the web engineers to implement.

![spec](https://storage.googleapis.com/public-assets-maypole/interview-assets/image%201.png)

Please scaffold the vue.js project in "hello-maypole" folder. As the lead of the project, please demonstrate:

* how the data is stored
* how the data is uploaded / downloaded
* how the page transition is handled
* how the key NPM packages are used
* etc.

Please write a document (in this folder) for both the web engineers & server side engineers to

* maximize their productivity
* clearly define the exit criteria