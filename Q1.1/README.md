Please finish this Firebase cloud function (https://firebase.google.com/docs/functions/get-started, specifically https://firebase.google.com/docs/functions/http-events) that load products from Shopify using Shopify's graphql (https://shopify.dev/docs/admin-api/graphql/reference/products-and-collections) and return them in json with basic product info like description, price, images etc. 

```javascript
// language: typescript
// environment: production
import { Request, Response } from 'express';
import { Application, Request, Router, RequestHandler, ErrorRequestHandler } from 'express';

export interface CloudFunctionRouteEntry {
  path: string;
  methods?: Array<'get' | 'post' | 'patch' | 'delete' | 'put'> | 'get' | 'post' | 'patch' | 'delete' | 'put';
  function: RequestHandler;
}

export const importShopifyProducts: CloudFunctionEntry = {
  path: '/q1/import/:shop/products',
  methods: ['post'],
  function: async (req: Request, res: Response) => {
    const { shop } = req.params;
    console.log('shop', shop);

    const { ids } = req.body as {
      ids: Array<string>; // the product ids to be loaded
    };
    
    ...
  },
};
```
